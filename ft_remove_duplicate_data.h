/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_remove_duplicate_data.h                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:27:22 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:05:52 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_REMOVE_DUPLICATE_DATA_H
# define FT_REMOVE_DUPLICATE_DATA_H

# include "structure.h"

void	ft_remove_duplicate_pipes(t_data *list);
void	ft_remove_duplicate_rooms(t_data *list);

#endif
