/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_graph.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 16:11:02 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/04 16:11:04 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_NODE_GRAPH_H
# define FT_INIT_NODE_GRAPH_H

# include "structure.h"

t_graph	*ft_init_node_graph(t_env *env, int s, int cost, int type);

#endif
