/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_map.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 12:23:10 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:03:06 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_NODE_MAP_H
# define FT_INIT_NODE_MAP_H

# include "structure.h"

t_map	*ft_init_node_map(t_env *env);

#endif
