/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_remove_node_data.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:05:18 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 16:05:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "structure.h"

void	ft_remove_node_data(t_data *list, t_data *node)
{
	t_data	*t;

	t = list;
	while (t->next != node)
		t = t->next;
	t->next = node->next;
	free(node);
}
