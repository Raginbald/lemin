/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:49:25 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:47:11 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"

t_data	*ft_init_node_data(t_env *env)
{
	t_data	*tmp;

	if (!(tmp = (t_data *)malloc(sizeof(t_data))))
		ft_exit(env, EXIT_FAILURE);
	tmp->id = 0;
	tmp->s1 = NULL;
	tmp->s2 = NULL;
	tmp->x = 0;
	tmp->y = 0;
	tmp->flag = 0;
	tmp->comment = NULL;
	tmp->command = NULL;
	tmp->next = NULL;
	return (tmp);
}
