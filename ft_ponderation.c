/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ponderation.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:09:42 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 16:09:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_get_coord.h"
#include "ft_name_to_id.h"

static float	ft_sqrt(float x)
{
	float		min;
	float		max;
	float		c;

	min = 0;
	c = 0;
	max = (x > 1) ? x : 1;
	while ((max - min) > 0.000001f)
	{
		c = (min + max) / 2;
		if (c * c > x)
			max = c;
		else
			min = c;
	}
	return (min);
}

void			ft_ponderation(t_env *env)
{
	t_data	*t;
	int		xa;
	int		xb;
	int		ya;
	int		yb;

	t = env->pipes;
	xa = 0;
	xb = 0;
	ya = 0;
	yb = 0;
	while (t)
	{
		ft_get_coord(env, ft_name_to_id(env, t->s1), &xa, &ya);
		ft_get_coord(env, ft_name_to_id(env, t->s2), &xb, &yb);
		t->id = ft_sqrt((xb - xa) * (xb - xa) + (yb - ya) * (yb - ya));
		t = t->next;
	}
}
