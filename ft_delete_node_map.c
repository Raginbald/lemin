/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_node_map.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:27:31 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:31:20 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_delete_node_map.h"

void	ft_delete_node_map(t_map *node)
{
	if (node->s)
		free(node->s);
	free(node);
	node = 0;
}
