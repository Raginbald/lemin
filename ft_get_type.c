/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_type.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:26:49 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/09 10:26:51 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_get_type.h"

int	ft_get_type(t_env *env, int id)
{
	t_data	*r;

	r = env->rooms;
	while (r)
	{
		if (r->id == id)
		{
			if (!r->command)
				return (0);
			else if (ft_strcmp(r->command, "##start") == 0)
			{
				env->start = r->id;
				return (1);
			}
			else if (ft_strcmp(r->command, "##end") == 0)
			{
				env->end = r->id;
				return (2);
			}
		}
		r = r->next;
	}
	return (-1);
}
