/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_union.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 15:47:19 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 15:47:20 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_find_union(int *father, int x, int y, int u)
{
	int	i;
	int	j;

	i = x;
	j = y;
	while (father[i] > -1)
		i = father[i];
	while (father[j] > -1)
		j = father[j];
	if ((u != 0 && i != j))
		father[j] = i;
	return (i != j);
}
