/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_num.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:50:44 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:49:46 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_get_num.h"

int	ft_get_num(t_env *env, t_read_data *i)
{
	int	nb;
	int start;

	nb = 0;
	start = 0;
	if (env)
	{
		while ((ft_iswhitespace(i->offset[i->c]) || i->offset[i->c] == '-')
				&& i->offset[i->c])
			i->c++;
		while (ft_isdigit(i->offset[i->c]) && i->offset[i->c])
		{
			nb = nb * 10 + (i->offset[i->c] - '0');
			i->c++;
		}
	}
	return (nb);
}
