/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_node_data.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 18:17:39 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:01:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DELETE_NODE_DATA_H
# define FT_DELETE_NODE_DATA_H

# include "structure.h"

void	ft_delete_node_data(t_data *node);

#endif
