/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_list_data.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 18:16:43 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:33:56 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_delete_node_data.h"

void	ft_delete_list_data(t_data *begin)
{
	t_data	*tmp;

	tmp = begin;
	while (begin)
	{
		tmp = begin;
		begin = begin->next;
		if (tmp)
			ft_delete_node_data(tmp);
		tmp = 0;
	}
}
