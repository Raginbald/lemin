/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_ant_nbr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:29:35 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:44:27 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_is_num.h"

int		ft_is_ant_nbr(t_read_data *i)
{
	int	test1;
	int	test2;

	if (i->ant == 1)
		return (0);
	test1 = ft_is_num(i);
	test2 = i->offset[i->c] == '\0';
	if (test1 && test2)
		return (1);
	return (0);
}
