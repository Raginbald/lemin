/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_data.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 10:53:00 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:11:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libftprintf.h>
#include "ft_print_data.h"

void	ft_print_map(t_map *node)
{
	if (node->s)
		ft_printf("%s\n", node->s);
}

void	ft_print_ants(t_data *node)
{
	if (node->comment)
		ft_list_foreach_map(node->comment, ft_print_map);
	if (node->command)
		ft_printf("%s\n", node->command);
	ft_printf("%d\n", (int)node->id);
}

void	ft_print_rooms(t_data *node)
{
	if (node->comment)
		ft_list_foreach_map(node->comment, ft_print_map);
	if (node->command)
		ft_printf("%s\n", node->command);
	ft_printf("%s %d %d\n", node->s1, node->x, node->y);
}

void	ft_print_pipes(t_data *node)
{
	if (node->comment)
		ft_list_foreach_map(node->comment, ft_print_map);
	if (node->command)
		ft_printf("%s\n", node->command);
	ft_printf("%s-%s\n", node->s1, node->s2);
}

void	ft_print_data(t_env *env)
{
	ft_list_foreach_data(env->ants, ft_print_ants);
	ft_list_foreach_data(env->rooms, ft_print_rooms);
	ft_list_foreach_data(env->pipes, ft_print_pipes);
}
