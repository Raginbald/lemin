/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_list_data.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 18:23:19 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/24 18:23:21 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DELETE_LIST_DATA_H
# define FT_DELETE_LIST_DATA_H

# include "structure.h"

void	ft_delete_list_data(t_data *begin);

#endif
