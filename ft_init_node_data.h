/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_data.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 11:09:41 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/25 11:23:24 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_NODE_DATA_H
# define FT_INIT_NODE_DATA_H

# include "structure.h"

t_data	*ft_init_node_data(t_env *env);

#endif
