/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structure.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 16:18:04 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:06:46 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTURE_H
# define STRUCTURE_H

# define EXIT_ERROR	2

typedef struct s_map		t_map;
typedef struct s_mapi		t_mapi;
typedef struct s_data		t_data;
typedef struct s_graph		t_graph;
typedef struct s_path		t_path;
typedef struct s_env		t_env;
typedef struct s_automaton	t_automaton;
typedef struct s_read_data	t_read_data;
typedef void				(*t_envf)(t_env *);
typedef int					(*t_parsef)(t_read_data *);
typedef void				(*t_getf)(t_env *, t_read_data *);

struct	s_env
{
	int		id;
	int		start;
	int		end;
	int		stop;
	float	last_cost;
	float	cost;
	int		max;
	int		nb_room;
	int		nb_pipe;
	int		*visited;
	t_data	*ants;
	t_data	*rooms;
	t_data	*pipes;
	t_graph	**graph;
	t_path	*paths;
	t_path	*shortest;
	int		*path;
	t_graph	*t;
};

struct	s_automaton
{
	t_parsef	ft_parse;
	t_getf		ft_get;
};

struct	s_data
{
	int		x;
	int		y;
	float	id;
	char	*s1;
	char	*s2;
	int		flag;
	t_map	*comment;
	char	*command;
	t_data	*next;
};

struct	s_mapi
{
	int		room;
	int		id;
};

struct	s_map
{
	char	*s;
	t_map	*next;
};

struct	s_graph
{
	int		s;
	int		flag;
	float	cost;
	int		type;
	t_graph	*next;
};

struct	s_path
{
	int		nb_node;
	float	cost;
	int		*path;
	t_path	*next;
};

struct	s_read_data
{
	int		i;
	int		c;
	char	ant;
	char	*offset;
	char	*current_command;
	t_map	*map;
};

#endif
