/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_tab_int.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 15:54:24 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:05:27 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_TAB_INT_H
# define FT_INIT_TAB_INT_H

# include "structure.h"

int	*ft_init_tab_int(t_env *env, int nb, int len);

#endif
