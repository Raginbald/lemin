/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/18 11:14:08 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/18 11:38:42 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include "ft_init_node_graph.h"
#include "ft_exit.h"

t_env	*ft_init_env(void)
{
	t_env	*tmp;

	if (!(tmp = (t_env *)malloc(sizeof(t_env))))
		ft_exit(tmp, EXIT_FAILURE);
	tmp->id = 0;
	tmp->start = -1;
	tmp->end = -1;
	tmp->stop = 0;
	tmp->nb_room = 0;
	tmp->nb_pipe = 0;
	tmp->last_cost = FLT_MAX;
	tmp->max = INT_MAX;
	tmp->cost = 0;
	tmp->visited = 0;
	tmp->ants = 0;
	tmp->rooms = 0;
	tmp->pipes = 0;
	tmp->graph = 0;
	tmp->paths = 0;
	tmp->shortest = 0;
	tmp->path = 0;
	tmp->t = 0;
	return (tmp);
}
