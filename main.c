/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 11:59:34 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/18 11:37:32 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_remove_duplicate_data.h"
#include "ft_id_to_name.h"
#include "ft_name_to_id.h"
#include "ft_exit.h"
#include "ft_print_data.h"
#include "main.h"

int		main(void)
{
	t_env	*env;

	env = ft_init_env();
	ft_read_data(env);
	ft_check_basic_error(env);
	ft_print_data(env);
	ft_remove_duplicate_rooms(env->rooms);
	ft_remove_duplicate_pipes(env->pipes);
	env->graph = ft_init_graph(env);
	ft_ponderation(env);
	ft_data_to_graph(env);
	ft_does_path_start_end_exist(env);
	ft_pathfinding(env);
	ft_maze_running(env);
	ft_exit(env, EXIT_SUCCESS);
	return (0);
}
