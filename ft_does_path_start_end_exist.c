/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_does_path_start_end_exist.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 15:46:09 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:04:12 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"
#include "ft_init_tab_int.h"
#include "ft_name_to_id.h"
#include "ft_find_union.h"
#include "structure.h"

void	ft_does_path_start_end_exist(t_env *env)
{
	int		*father;
	int		i;
	int		j;
	int		u;
	t_data	*t;

	i = 0;
	j = 0;
	u = 1;
	t = env->pipes;
	father = ft_init_tab_int(env, -1, env->nb_room);
	while (t)
	{
		i = ft_name_to_id(env, t->s1);
		j = ft_name_to_id(env, t->s2);
		u = ft_find_union(father, i, j, u);
		t = t->next;
	}
	u = ft_find_union(father, env->start, env->end, u);
	free(father);
	father = 0;
	if (u == 1)
		ft_exit(env, EXIT_ERROR);
}
