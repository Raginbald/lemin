/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_node_map.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 12:35:54 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/24 12:39:03 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DELETE_NODE_MAP_H
# define FT_DELETE_NODE_MAP_H

# include "structure.h"

void	ft_delete_node_map(t_map *node);

#endif
