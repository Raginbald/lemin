/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pathfinding.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 15:48:06 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 18:59:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_get_shortest_path.h"
#include "ft_remove_useless_path.h"
#include "ft_add_new_path.h"
#include "ft_init_node_path.h"
#include "ft_init_tab_int.h"
#include "ft_id_to_name.h"
#include "ft_exit.h"

static void	ft_explorer(t_env *env, int s, int *path)
{
	t_graph	*t;

	env->cost += env->graph[s]->cost;
	env->visited[s] = 1;
	path[env->id] = s;
	env->id++;
	t = env->graph[s];
	if (s == env->end)
		ft_add_new_path(env, path);
	else if (env->id < env->max && env->cost < env->last_cost)
	{
		while (t->next)
		{
			if (env->visited[t->s] == 0)
				ft_explorer(env, t->s, path);
			t = t->next;
		}
	}
	env->id--;
	env->visited[s] = 0;
	env->cost -= env->graph[s]->cost;
}

void		ft_pathfinding(t_env *env)
{
	int		*path;

	env->id = 0;
	env->visited = ft_init_tab_int(env, 0, env->nb_room);
	path = ft_init_tab_int(env, -1, env->nb_room);
	ft_explorer(env, env->start, path);
	ft_get_shortest_path(env, env->paths);
	free(env->visited);
	free(path);
}
