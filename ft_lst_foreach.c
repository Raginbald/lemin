/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_foreach.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 12:35:32 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:35:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "structure.h"

void	ft_list_foreach_map(t_map *node, void (*f)(t_map *))
{
	if (node)
	{
		f(node);
		if (node->next)
			ft_list_foreach_map(node->next, f);
	}
}

void	ft_list_foreach_data(t_data *node, void (*f)(t_data *))
{
	if (node)
	{
		f(node);
		if (node->next)
			ft_list_foreach_data(node->next, f);
	}
}
