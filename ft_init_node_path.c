/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_path.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 18:44:23 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:01:36 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"
#include "ft_init_tab_int.h"

t_path	*ft_init_node_path(t_env *env, int nb_node, int cost, int *path)
{
	t_path	*tmp;
	int		*tab;
	int		i;

	i = 0;
	if (!(tmp = (t_path *)malloc(sizeof(t_path))))
		ft_exit(env, EXIT_FAILURE);
	tmp->nb_node = nb_node;
	tmp->cost = cost;
	tmp->next = NULL;
	tab = ft_init_tab_int(env, 0, nb_node);
	while (i < nb_node)
	{
		tab[i] = path[i];
		++i;
	}
	tmp->path = tab;
	return (tmp);
}
