/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_shortest_path.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 15:30:17 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/12 15:30:18 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GET_SHORTEST_PATH_H
# define FT_GET_SHORTEST_PATH_H

# include "structure.h"

void		ft_get_shortest_path(t_env *env, t_path *list);

#endif
