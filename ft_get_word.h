/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_word.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 14:42:26 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/04 14:42:28 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GET_WORD_H
# define FT_GET_WORD_H

# include "structure.h"

char	*ft_get_word(t_env *env, t_read_data *i);

#endif
