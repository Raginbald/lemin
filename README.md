# lemin

USAGE<br>
./a.out < data.txt

naive pathfinding<br>
faire parcourir des entités dans un labyrinthe

creation d'un graphe à partir des donneés ordonneé comme suivant :<br>
-le nombre d'entité (un nombre positif)<br>
-##commande<br>
-#commentaire<br>
-les noeud (un id et deux coordonnée x y)<br>
-les connexions (id-id)

42            // le nombre d'entités<br>
1 36 17       // le nom des noeuds suivi de leur coordonnées x y<br>
##start       // une commande pour indiquer le premier noeud<br>
2 38 9        // par lequel les entités débuteront<br>
3 45 13<br>
4 75 27<br>
##end         // la fin<br>
5 36 303<br>
6 66 88<br>
2-1           // les connexions entres les diffeérents noeud<br>
2-4<br>
3-6<br>
3-5<br>
4-1<br>
4-6<br>
5-6<br>
6-1<br>

le but : création/manipulation de graphes,<br>
trouver le plus court chemin entre start et end

l'algorithme est fait maison ce n'est pas djikstra ou A*<br>
j'ai utilisé un heuristique très simple.

DFS pour parcourir tout les noeuds,<br>
Appartenance/union pour vérifier si un ou plusieurs noeuds<br>
appartiennent au même graphe,<br>
connexité pour vérifier si il est possible d'avoir plusieurs<br>
chemin différent accessible au même momment
