/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_width.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:31:18 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:07:12 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		ft_check_if_negative_width(int *w, t_printf *env)
{
	if (*w < 0)
	{
		*w = -(*w);
		env->flag |= FLAG_LEFT;
	}
}

void			ft_get_width(va_list args, const char *format, t_printf *env)
{
	int w;

	w = 0;
	if (format[env->i] == '*')
	{
		env->flag |= FLAG_WIDTH;
		w = va_arg(args, int);
		ft_check_if_negative_width(&w, env);
		env->width = ft_maxp(w, env->width);
		if (w == 0)
			env->width = 0;
		++env->i;
	}
	else if (format[env->i] >= '1' && format[env->i] <= '9')
	{
		env->flag |= FLAG_WIDTH;
		env->width = ft_atoip(&format[env->i]);
		while (format[env->i] >= '0' && format[env->i] <= '9')
			++env->i;
	}
	if (format[env->i] == '*' || (format[env->i] >= '0'
									&& format[env->i] <= '9'))
		ft_get_width(args, format, env);
}
