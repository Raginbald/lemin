/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_qualifier.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:27:32 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:04:15 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static int	ft_isqualifier(int c)
{
	return (c == 'h' || c == 'l' || c == 'j' || c == 'z');
}

void		ft_get_qualifier(const char *format, t_printf *env)
{
	if (ft_isqualifier(format[env->i]))
	{
		env->qualifier = format[env->i];
		++env->i;
		if (format[env->i] == 'h')
		{
			env->qualifier = 'H';
			++env->i;
		}
		else if (format[env->i] == 'l')
		{
			env->qualifier = 'L';
			++env->i;
		}
	}
}
