/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoip.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 18:25:24 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 12:33:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int	ft_iswhitespace(int c)
{
	return ((c == 32) || (c >= 9 && c <= 13));
}

int			ft_atoip(const char *str)
{
	int			neg;
	int			i;
	int			n;

	if (!str)
		return (0);
	i = 0;
	n = 0;
	neg = 1;
	while (ft_iswhitespace(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			neg = -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		n = n * 10 + (str[i] - '0');
		i++;
	}
	return (n * neg);
}
