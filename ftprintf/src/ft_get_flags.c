/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_flags.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:27:02 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 14:58:52 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_get_flags(const char *format, t_printf *env)
{
	int	i;

	i = 0;
	while (format[env->i])
	{
		i = 0;
		while (FLAG_STRING[i])
		{
			if (format[env->i] == FLAG_STRING[i])
			{
				env->flag |= (1 << i);
				break ;
			}
			++i;
		}
		if (!FLAG_STRING[i])
			return ;
		++env->i;
	}
}
