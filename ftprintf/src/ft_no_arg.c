/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_no_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/14 16:53:27 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 15:08:39 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	ft_no_arg(t_printf *env, va_list args)
{
	char	padchar;

	padchar = (env->flag & FLAG_ZERO) ? '0' : ' ';
	(void)args;
	env->len = 1;
	if (env->precision != -1 && env->len > env->precision)
		env->len = env->precision;
	if (!(env->flag & FLAG_LEFT))
	{
		while (env->len < env->width--)
			env->printed += ft_putcharp(padchar);
	}
	env->printed += ft_putcharp(env->specifier);
	while (env->len < env->width--)
		env->printed += ft_putcharp(' ');
	env->i = env->backup;
}
