/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:26:07 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:26:11 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_arg_p(t_printf *env, va_list args)
{
	env->num = (unsigned long)va_arg(args, void *);
	env->flag |= FLAG_UNBR + FLAG_ALTERNATE;
	env->base = 16;
	env->len += ft_u_numberlen(env->num, env->base);
	env->str = ft_u_itoa_base(env, env->num, env->base);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}
