/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrlen_pf.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/21 16:04:25 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 16:04:26 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_wstrlen_pf(wchar_t *wstr)
{
	int	i;

	i = 0;
	while (*wstr)
	{
		i += ft_wclen_pf(*wstr);
		++wstr;
	}
	return (i);
}
