/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_b.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 11:19:55 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/29 11:35:29 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

static char	*ft_negative_binary(int *flag, char *str)
{
	int		i;
	char	*tmp;

	i = 0;
	if (!(tmp = (char *)malloc(sizeof(char) * ft_strlenp(str) + 1)))
		ft_exitp(2, ERROR_MALLOC);
	tmp[i] = '1';
	while (str[i])
	{
		tmp[i + 1] = (str[i] == '1') ? '0' : '1';
		++i;
	}
	free(str);
	str = NULL;
	*flag &= ~FLAG_SNBR;
	return (tmp);
}

void		ft_arg_b(t_printf *env, va_list args)
{
	env->base = 2;
	env->flag |= FLAG_SNBR;
	ft_arg_needler(args, env, env->tab_qual, env->qualifier);
	env->len += ft_s_numberlen(env->snum, env->base);
	env->str = ft_s_itoa_base(env, env->snum, env->base);
	if (env->flag & FLAG_ISNEG)
		env->str = ft_negative_binary(&env->flag, env->str);
	if (env->flag & FLAG_NO_PRECISION)
	{
		if (env->str)
			free(env->str);
		env->str = NULL;
	}
	ft_print_number(env);
}
