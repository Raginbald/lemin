/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wclen_pf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/21 16:04:11 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 16:04:13 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_wclen_pf(wchar_t wc)
{
	int	i;

	i = 0;
	if (wc < 0x80)
		++i;
	else if (wc < 0x800)
		i += 2;
	else if (wc < 0x10000)
		i += 3;
	else if (wc < 0x200000)
		i += 4;
	return (i);
}
