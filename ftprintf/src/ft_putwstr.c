/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/09 14:02:16 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/09 14:03:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_putwstr_len(const wchar_t *str, int len)
{
	int i;

	i = 0;
	if (str == NULL)
		return (0);
	while (len > 0)
	{
		i += ft_putwchar(*str++);
		--len;
	}
	return (i);
}

int	ft_putwstr(const wchar_t *str)
{
	int i;

	i = 0;
	while (*str)
		i += ft_putwchar(*str++);
	return (i);
}
