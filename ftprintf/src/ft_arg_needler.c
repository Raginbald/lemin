/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_needler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 12:44:12 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/21 16:37:25 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftprintf.h"

void	ft_arg_needler(va_list args, t_printf *env, t_arg_pf *tab, char c)
{
	int			i;

	i = 0;
	while (tab[i].op && tab[i].op != c)
		++i;
	tab[i].pf(env, args);
}
