/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/07 10:53:54 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/07 11:04:12 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINE_H
# define DEFINE_H

# define FLAG_ALTERNATE	1
# define FLAG_ZERO		2
# define FLAG_LEFT		4
# define FLAG_SPACE		8
# define FLAG_PLUS		16
# define FLAG_SIGN		32
# define FLAG_STR		64
# define FLAG_UNBR 		128
# define FLAG_SNBR		2048
# define FLAG_ISNEG		256
# define FLAG_PRECISION 512
# define FLAG_WIDTH		1024
# define MASK_STR_PRECI 576
# define FLAG_NO_PRECISION 4096
# define SPECIFIER_STR	"dDioOuUxXbcCsSp"
# define QUALIFIER_STR	"dHhlLjz"
# define FLAG_STRING	"#0- +"
# define BASE_STR 		"0123456789abcdefx"
# define BASE_STR_UP 	"0123456789ABCDEFX"
# define MISSING_SPECIFIER "exit(failure) : missing specifier\n"
# define MISSING_PRECISION "exit(failure) : missing precision\n"
# define ERROR_MALLOC "exit(failure) : bad allocation\n"
# define OVERRIDE_FLAG_1 "flag '0' is ignored when flag '-' is present\n"
# define OVERRIDE_FLAG_2 "flag ' ' is ignored when flag '+' is present\n"

#endif
