/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_word.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 15:47:09 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/04 15:47:11 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_is_word.h"

int		ft_is_word(t_read_data *i)
{
	int	d;

	d = 0;
	while (ft_isalnum(i->offset[i->c]))
	{
		++d;
		++i->c;
	}
	if (d)
		return (1);
	return (0);
}
