/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_new_path.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 11:53:20 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/12 11:53:22 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_init_node_path.h"

void	ft_add_new_path(t_env *env, int *path)
{
	t_path *node;

	node = ft_init_node_path(env, env->id, env->cost, path);
	node->next = env->paths;
	env->paths = node;
	env->max = env->id;
	env->last_cost = env->cost;
}
