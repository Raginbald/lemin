/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_tab_int.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 15:50:04 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:01:11 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"

int	*ft_init_tab_int(t_env *env, int nb, int len)
{
	int		*tmp;
	int		i;

	i = 0;
	if (!(tmp = (int *)malloc(sizeof(int) * len)))
		ft_exit(env, 1);
	while (i < len)
	{
		tmp[i] = nb;
		++i;
	}
	return (tmp);
}
