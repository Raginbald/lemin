/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_graph.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 12:58:47 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:46:55 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"

t_graph	**ft_init_graph(t_env *env)
{
	t_graph	**tab;
	t_graph *z;
	int		i;

	i = 0;
	if (!(tab = (t_graph **)malloc(sizeof(t_graph *) * env->nb_room)))
		ft_exit(env, EXIT_FAILURE);
	if (!(z = (t_graph *)malloc(sizeof(t_graph))))
		ft_exit(env, EXIT_FAILURE);
	z->s = -2;
	z->type = -1;
	z->flag = -1;
	z->next = NULL;
	while (i < env->nb_room)
		tab[i++] = z;
	return (tab);
}
