/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_coord.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:11:55 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 16:11:57 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GET_COORD_H
# define FT_GET_COORD_H

# include "structure.h"

void	ft_get_coord(t_env *env, int id, int *x, int *y);

#endif
