/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_shortest_path.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 15:26:40 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/12 15:26:42 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "structure.h"

static void	ft_get_shortest_nb_of_node(t_env *env, t_path *list)
{
	t_path *t;

	t = list;
	while (t)
	{
		if (t != env->shortest && t->cost == env->shortest->cost)
		{
			if (t->nb_node < env->shortest->nb_node)
			{
				env->max = t->nb_node;
				env->last_cost = t->cost;
				env->shortest = t;
			}
		}
		t = t->next;
	}
}

void		ft_get_shortest_path(t_env *env, t_path *list)
{
	t_path *t;

	t = list;
	while (t)
	{
		if (t->cost <= env->last_cost)
		{
			env->max = t->nb_node;
			env->last_cost = t->cost;
			env->shortest = t;
		}
		t = t->next;
	}
	ft_get_shortest_nb_of_node(env, env->paths);
}
