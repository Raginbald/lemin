/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_basic_error.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 15:56:24 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/12 15:56:25 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_exit.h"
#include "structure.h"

void	ft_check_basic_error(t_env *env)
{
	char a;

	a = 0;
	a |= env->nb_room < 2;
	a |= env->start == -1 || env->end == -1;
	if (!env->ants)
		ft_exit(env, 1);
	a |= env->ants->id <= 0;
	a |= env->nb_pipe <= 0;
	if (a)
		ft_exit(env, 1);
}
