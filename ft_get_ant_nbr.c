/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_ant_nbr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 13:39:53 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/24 14:38:02 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_init_read_data.h"
#include "ft_delete_list_map.h"
#include "ft_init_node_data.h"

void	ft_get_ant_nbr(t_env *env, t_read_data *i)
{
	t_data	*ant;

	ant = ft_init_node_data(env);
	ant->id = ft_atoi(i->offset);
	ant->comment = i->map;
	ant->command = i->current_command;
	env->ants = ant;
	ft_init_read_data(i);
	i->ant = 1;
}
