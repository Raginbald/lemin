/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_room.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:28:46 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/24 11:28:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_is_num.h"
#include "ft_is_word.h"
#include "ft_is_space.h"

int			ft_is_room(t_read_data *i)
{
	if (ft_is_word(i) && ft_is_space(i)
		&& ft_is_num(i) && ft_is_space(i)
		&& ft_is_num(i) && i->offset[i->c] == '\0')
	{
		return (1);
	}
	return (0);
}
