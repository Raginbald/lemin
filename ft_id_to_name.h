/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_id_to_name.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:49:30 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/09 10:49:32 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ID_TO_NAME_H
# define FT_ID_TO_NAME_H

# include "structure.h"

char *ft_id_to_name(t_env *env, int id);

#endif
