/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_room.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:50:23 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:04:39 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_get_num.h"
#include "ft_get_word.h"
#include "ft_init_read_data.h"
#include "ft_init_node_data.h"

static void	ft_start_end_commands(t_env *env, t_data *room)
{
	if (room->command)
	{
		if (!ft_strcmp(room->command, "##start"))
			env->start = room->id;
		else if (!ft_strcmp(room->command, "##end"))
			env->end = room->id;
	}
}

void		ft_get_room(t_env *env, t_read_data *i)
{
	t_data	*room;
	t_data	*it;

	it = env->rooms;
	room = ft_init_node_data(env);
	room->id = env->nb_room++;
	room->s1 = ft_get_word(env, i);
	room->x = ft_get_num(env, i);
	room->y = ft_get_num(env, i);
	room->comment = i->map;
	room->command = i->current_command;
	ft_start_end_commands(env, room);
	if (env->rooms)
	{
		while (it->next)
			it = it->next;
		it->next = room;
	}
	else
		env->rooms = room;
	ft_init_read_data(i);
}
