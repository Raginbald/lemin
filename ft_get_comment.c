/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_comment.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:50:05 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:27:04 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_init_node_map.h"

void	ft_get_comment(t_env *env, t_read_data *i)
{
	t_map	*x;
	t_map	*it;

	x = ft_init_node_map(env);
	x->s = ft_strdup(i->offset);
	it = i->map;
	if (i->map)
	{
		while (it->next)
			it = it->next;
		it->next = x;
	}
	else
		i->map = x;
}
