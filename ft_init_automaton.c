/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_automaton.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:28:18 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:47:36 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"
#include "ft_init_automaton.h"

t_automaton	*ft_init_automaton(t_env *env)
{
	t_automaton	*tmp;

	if (!(tmp = (t_automaton *)malloc(sizeof(t_automaton) * 6)))
		ft_exit(env, EXIT_FAILURE);
	tmp[0].ft_parse = ft_is_ant_nbr;
	tmp[0].ft_get = ft_get_ant_nbr;
	tmp[1].ft_parse = ft_is_comment;
	tmp[1].ft_get = ft_get_comment;
	tmp[2].ft_parse = ft_is_command;
	tmp[2].ft_get = ft_get_command;
	tmp[3].ft_parse = ft_is_room;
	tmp[3].ft_get = ft_get_room;
	tmp[4].ft_parse = ft_is_pipe;
	tmp[4].ft_get = ft_get_pipe;
	tmp[5].ft_parse = ft_is_useless;
	tmp[5].ft_get = ft_void_useless;
	return (tmp);
}
