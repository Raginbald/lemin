/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_name_to_id.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:49:43 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/09 10:49:45 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_NAME_TO_ID_H
# define FT_NAME_TO_ID_H

# include "structure.h"

int ft_name_to_id(t_env *env, char *str);

#endif
