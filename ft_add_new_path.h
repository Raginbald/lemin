/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_new_path.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 11:54:47 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/12 11:54:48 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ADD_NEW_PATH_H
# define FT_ADD_NEW_PATH_H

# include "structure.h"

void	ft_add_new_path(t_env *env, int *path);

#endif
