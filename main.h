/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 11:59:52 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/18 11:39:11 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

void	ft_check_basic_error(t_env *env);
void	ft_data_to_graph(t_env *env);
void	ft_does_path_start_end_exist(t_env *env);
t_env	*ft_init_env(void);
t_graph	**ft_init_graph(t_env *env);
void	ft_maze_running(t_env *env);
void	ft_pathfinding(t_env *env);
void	ft_ponderation(t_env *env);
void	ft_read_data(t_env *env);

#endif
