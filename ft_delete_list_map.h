/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_list_map.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:49:03 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/24 11:51:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DELETE_LIST_MAP_H
# define FT_DELETE_LIST_MAP_H

# include "structure.h"

void	ft_delete_list_map(t_map *begin);

#endif
