/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_node_data.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:27:10 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:29:50 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "ft_delete_list_map.h"

void	ft_delete_node_data(t_data *node)
{
	node->id = 0;
	node->x = 0;
	node->y = 0;
	if (node->comment)
		ft_delete_list_map(node->comment);
	if (node->command)
		ft_strdel(&node->command);
	if (node->s1)
		ft_strdel(&node->s1);
	if (node->s2)
		ft_strdel(&node->s2);
	free(node);
	node = 0;
}
