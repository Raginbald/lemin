/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_id_to_name.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:27:13 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:02:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_id_to_name.h"

char	*ft_id_to_name(t_env *env, int id)
{
	t_data *r;

	r = env->rooms;
	while (r)
	{
		if (r->id == id)
			return (r->s1);
		r = r->next;
	}
	return (0);
}
