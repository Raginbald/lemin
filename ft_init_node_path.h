/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_path.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 19:14:50 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 19:14:51 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_NODE_PATH_H
# define FT_INIT_NODE_PATH_H

# include "structure.h"

t_path	*ft_init_node_path(t_env *env, int nb_node, int cost, int *path);

#endif
