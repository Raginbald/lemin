/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_command.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:50:13 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:29:26 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "structure.h"

void	ft_get_command(t_env *env, t_read_data *i)
{
	if (env)
		i->current_command = ft_strdup(i->offset);
}
