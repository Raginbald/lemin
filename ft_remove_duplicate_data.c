/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_remove_duplicate_data.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:03:35 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 16:03:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_remove_node_data.h"

static int	ft_duplicate_or_invalid_room(char *s1, char *s2)
{
	if (!ft_strcmp(s1, s2) || s1[0] == 'L')
		return (1);
	return (0);
}

void		ft_remove_duplicate_rooms(t_data *list)
{
	t_data	*t;
	t_data	*offset;

	offset = list;
	t = offset->next;
	while (offset)
	{
		t = offset->next;
		while (t)
		{
			if (ft_duplicate_or_invalid_room(t->s1, offset->s1))
				ft_remove_node_data(list, t);
			t = t->next;
		}
		offset = offset->next;
	}
}

static int	ft_duplicate_or_invalid_pipe(char *s1, char *s2, char *s3, char *s4)
{
	if ((!ft_strcmp(s1, s4) && !ft_strcmp(s2, s3))
		|| s1[0] == 'L' || s2[0] == 'L')
		return (1);
	return (0);
}

void		ft_remove_duplicate_pipes(t_data *list)
{
	t_data	*t;
	t_data	*offset;

	offset = list;
	t = offset->next;
	while (offset)
	{
		t = offset->next;
		while (t)
		{
			if (ft_duplicate_or_invalid_pipe(t->s1,
												t->s2,
												offset->s1,
												offset->s2))
				ft_remove_node_data(list, t);
			t = t->next;
		}
		offset = offset->next;
	}
}
