/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_automaton.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 13:35:00 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:03:46 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_AUTOMATON_H
# define FT_INIT_AUTOMATON_H

# include "structure.h"

t_automaton	*ft_init_automaton(t_env *env);
int			ft_is_ant_nbr(t_read_data *i);
int			ft_is_comment(t_read_data *i);
int			ft_is_command(t_read_data *i);
int			ft_is_room(t_read_data *i);
int			ft_is_pipe(t_read_data *i);
int			ft_is_useless(t_read_data *i);
void		ft_get_ant_nbr(t_env *env, t_read_data *i);
void		ft_get_command(t_env *env, t_read_data *i);
void		ft_get_comment(t_env *env, t_read_data *i);
void		ft_get_room(t_env *env, t_read_data *i);
void		ft_get_pipe(t_env *env, t_read_data *i);
void		ft_void_useless(t_env *env, t_read_data *i);

#endif
