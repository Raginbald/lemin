/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_remove_node_data.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:33:46 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 16:33:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_REMOVE_NODE_DATA_H
# define FT_REMOVE_NODE_DATA_H

# include "structure.h"

void	ft_remove_node_data(t_data *list, t_data *node);

#endif
