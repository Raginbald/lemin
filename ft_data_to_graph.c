/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_data_to_graph.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 12:25:38 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:31:06 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_get_type.h"
#include "ft_name_to_id.h"
#include "ft_init_node_graph.h"
#include "structure.h"

void		ft_data_to_graph(t_env *env)
{
	t_graph	*t;
	t_data	*it;
	int		x;
	int		y;

	it = env->pipes;
	x = 0;
	y = 0;
	while (it)
	{
		x = ft_name_to_id(env, it->s1);
		y = ft_name_to_id(env, it->s2);
		t = ft_init_node_graph(env, x, it->id, ft_get_type(env, x));
		t->next = env->graph[y];
		env->graph[y] = t;
		t = ft_init_node_graph(env, y, it->id, ft_get_type(env, y));
		t->next = env->graph[x];
		env->graph[x] = t;
		it = it->next;
	}
	t = 0;
}
