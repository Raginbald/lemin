/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_map.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:49:16 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:45:20 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"

t_map	*ft_init_node_map(t_env *env)
{
	t_map	*tmp;

	if (!(tmp = (t_map *)malloc(sizeof(t_map))))
		ft_exit(env, EXIT_FAILURE);
	tmp->s = NULL;
	tmp->next = NULL;
	return (tmp);
}
