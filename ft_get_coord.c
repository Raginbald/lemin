/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_coord.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/11 16:10:44 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/11 16:10:45 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "structure.h"

void	ft_get_coord(t_env *env, int id, int *x, int *y)
{
	t_data *r;

	r = env->rooms;
	while (r)
	{
		if (r->id == id)
		{
			*x = r->x;
			*y = r->y;
		}
		r = r->next;
	}
}
