/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_pipe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 13:50:32 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/20 13:50:36 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "ft_get_word.h"
#include "ft_init_read_data.h"
#include "ft_init_node_data.h"

static int	ft_room_doesnt_exist(t_data *room, t_data *pipe)
{
	t_data	*offset;
	char	test1;
	char	test2;

	offset = room;
	while (offset)
	{
		if (!ft_strcmp(pipe->s1, offset->s1))
			test1 = 1;
		if (!ft_strcmp(pipe->s2, offset->s1))
			test2 = 1;
		offset = offset->next;
	}
	if (test1 == 0 || test2 == 0)
		return (1);
	return (0);
}

void		ft_get_pipe(t_env *env, t_read_data *i)
{
	t_data	*pipe;
	t_data	*it;

	it = env->pipes;
	pipe = ft_init_node_data(env);
	pipe->s1 = ft_get_word(env, i);
	pipe->s2 = ft_get_word(env, i);
	if (ft_room_doesnt_exist(env->rooms, pipe))
	{
		free(pipe);
		return ;
	}
	pipe->comment = i->map;
	pipe->command = i->current_command;
	if (env->pipes)
	{
		while (it->next)
			it = it->next;
		it->next = pipe;
	}
	else
		env->pipes = pipe;
	ft_init_read_data(i);
	env->nb_pipe++;
}
