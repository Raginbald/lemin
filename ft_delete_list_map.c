/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_delete_list_map.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:27:23 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 13:32:38 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_delete_node_map.h"

void	ft_delete_list_map(t_map *begin)
{
	t_map *tmp;

	tmp = begin;
	while (begin)
	{
		tmp = begin;
		begin = begin->next;
		if (tmp)
			ft_delete_node_map(tmp);
		tmp = 0;
	}
}
