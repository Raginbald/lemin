# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/12 13:05:56 by graybaud          #+#    #+#              #
#    Updated: 2016/03/17 18:06:41 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= lemin-in
DEBUG	= debug-lemin
CC		= clang
LIBFT	= libft/libft.a
LBPRINTF= ftprintf/libftprintf.a
IFLAG	= -I ./ -I ./libft/ -I ./ftprintf/includes 
WFLAG	= -Wall -Werror -Wextra
LFLAG	= -L ./libft/ -lft -L ./ftprintf/ -lftprintf
RM		= rm -Rf
NCOL	= \x1b[0m
RED		= \x1b[31;01m
VERT	= \x1b[32;01m
JAUNE	= \x1b[33;01m
H		= main.h					\
			structure.h				\
			ft_add_new_path.h		\
			ft_delete_list_map.h	\
			ft_delete_node_map.h 	\
			ft_exit.h				\
			ft_find_union.h			\
			ft_get_num.h			\
			ft_get_coord.h			\
			ft_get_shortest_path.h	\
			ft_get_type.h			\
			ft_get_word.h 			\
			ft_id_to_name.h			\
			ft_is_num.h				\
			ft_is_word.h 			\
			ft_init_node_data.h 	\
			ft_init_automaton.h 	\
			ft_init_node_map.h		\
			ft_init_node_graph.h	\
			ft_init_node_path.h		\
			ft_init_read_data.h		\
			ft_init_tab_int.h		\
			ft_is_space.h 			\
			ft_name_to_id.h			\
			ft_print_data.h			\
			ft_remove_duplicate_data.h	\
			ft_remove_node_data.h	\

SRC		= main.c 					\
			ft_add_new_path.c		\
			ft_check_basic_error.c	\
			ft_data_to_graph.c		\
			ft_delete_list_map.c 	\
			ft_delete_list_data.c	\
			ft_delete_node_data.c	\
			ft_delete_node_map.c	\
			ft_does_path_start_end_exist.c	\
			ft_exit.c 				\
			ft_find_union.c			\
			ft_get_ant_nbr.c 		\
			ft_get_command.c 		\
			ft_get_comment.c 		\
			ft_get_coord.c			\
			ft_get_num.c 			\
			ft_get_shortest_path.c	\
			ft_get_pipe.c			\
			ft_get_type.c 			\
			ft_get_room.c 			\
			ft_get_word.c 			\
			ft_id_to_name.c			\
			ft_init_automaton.c		\
			ft_init_env.c 			\
			ft_init_graph.c 		\
			ft_init_node_graph.c	\
			ft_init_node_map.c 		\
			ft_init_node_data.c		\
			ft_init_node_path.c		\
			ft_init_read_data.c		\
			ft_init_tab_int.c		\
			ft_is_ant_nbr.c			\
			ft_is_command.c 		\
			ft_is_comment.c		 	\
			ft_is_num.c 			\
			ft_is_pipe.c			\
			ft_is_room.c 			\
			ft_is_space.c			\
			ft_is_useless.c 		\
			ft_is_word.c 			\
			ft_lst_foreach.c		\
			ft_name_to_id.c			\
			ft_maze_running.c		\
			ft_pathfinding.c		\
			ft_print_data.c 		\
			ft_ponderation.c		\
			ft_read_data.c			\
			ft_remove_duplicate_data.c	\
			ft_remove_node_data.c	\
			ft_void_useless.c

OBJ		= $(SRC:.c=.o)

$(DEBUG): $(LIBFT) $(LBPRINTF) $(OBJ) $(H)
	@$(CC) $(WFLAG) -g $(LFLAG) $(IFLAG) $(SRC) -o $@
	@echo "debug version"


$(NAME): $(LIBFT) $(LBPRINTF) $(OBJ) $(H)
	@$(CC) $(WFLAG) $(LFLAG) $(IFLAG) $(OBJ) -o $@
	@echo "executable ready"

$(LIBFT):
	@(cd libft/ && $(MAKE))

$(LBPRINTF):
	@(cd ftprintf/ && $(MAKE))

%.o:%.c
	@$(CC) $(WFLAG) $(IFLAG) -o $@ -c $<

all: $(NAME)

debug:$(DEBUG)

clean:
	@(cd libft/ && $(MAKE) $@)
	@(cd ftprintf/ && $(MAKE) $@)
	@$(RM) $(OBJ)

dclean:
	@$(RM) $(DEBUG).dSYM $(DEBUG)

fclean: clean
	@(cd libft/ && $(MAKE) $@)
	@(cd ftprintf/ && $(MAKE) $@)
	@$(RM) $(NAME)

re: fclean all

.PHONY: all, clean, fclean, re
