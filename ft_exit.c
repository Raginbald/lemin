/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/18 11:10:51 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/18 11:36:42 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <libftprintf.h>
#include "ft_delete_list_data.h"
#include "ft_delete_node_data.h"
#include "ft_exit.h"

static void	ft_remove_data(t_env *env)
{
	if (env->ants)
		ft_delete_node_data(env->ants);
	if (env->rooms)
		ft_delete_list_data(env->rooms);
	if (env->pipes)
		ft_delete_list_data(env->pipes);
}

static void	ft_delete_env(t_env *env)
{
	ft_remove_data(env);
	free(env);
	env = 0;
}

void		ft_exit(t_env *env, int flag)
{
	if (env)
		ft_delete_env(env);
	if (flag == 1)
		ft_printf("%s\n", strerror(errno));
	if (flag == 2)
		ft_printf("Error\n");
	exit(flag);
}
