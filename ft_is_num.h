/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_num.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 12:45:27 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/24 12:47:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_IS_NUM_H
# define FT_IS_NUM_H

# include "structure.h"

int	ft_is_num(t_read_data *i);
int	ft_is_space(t_read_data *i);

#endif
