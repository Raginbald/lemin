/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_data.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 12:34:17 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/26 12:35:51 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINT_DATA_H
# define FT_PRINT_DATA_H

# include "structure.h"

void	ft_print_map(t_map *node);
void	ft_print_ants(t_data *node);
void	ft_print_rooms(t_data *node);
void	ft_print_pipes(t_data *node);
void	ft_list_foreach_data(t_data *node, void (*f)(t_data *));
void	ft_list_foreach_map(t_map *node, void (*f)(t_map *));
void	ft_print_data(t_env *env);

#endif
