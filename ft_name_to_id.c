/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_name_to_id.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 10:27:01 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 19:00:10 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "structure.h"

int	ft_name_to_id(t_env *env, char *str)
{
	t_data	*r;

	r = env->rooms;
	while (r)
	{
		if (ft_strcmp(r->s1, str) == 0)
			return (r->id);
		r = r->next;
	}
	return (-1);
}
