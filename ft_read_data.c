/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_data.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 11:28:01 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:40:55 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "ft_init_read_data.h"
#include "ft_init_automaton.h"

static void	ft_check_last_line(t_env *env, t_read_data *i, t_automaton *tab)
{
	if (!i->offset || i->offset[0] == '\0' || i->offset[0] == '\n')
	{
		free(tab);
		return ;
	}
	ft_epur_str(i->offset);
	while (!tab[i->i].ft_parse(i))
	{
		i->c = 0;
		++i->i;
	}
	i->c = 0;
	tab[i->i].ft_get(env, i);
	i->i = 0;
	if (i->offset)
		ft_strdel(&i->offset);
	free(tab);
}

void		ft_read_data(t_env *env)
{
	t_read_data	i;
	t_automaton	*tab;

	i.ant = 0;
	ft_init_read_data(&i);
	tab = ft_init_automaton(env);
	while (42)
	{
		ft_get_next_line(0, &i.offset);
		if (!i.offset || i.offset[0] == '\0' || i.offset[0] == '\n')
			break ;
		ft_epur_str(i.offset);
		while (!tab[i.i].ft_parse(&i))
		{
			i.c = 0;
			++i.i;
		}
		i.c = 0;
		tab[i.i].ft_get(env, &i);
		i.i = 0;
		if (i.offset)
			ft_strdel(&i.offset);
	}
	ft_check_last_line(env, &i, tab);
}
