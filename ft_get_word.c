/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_word.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 14:36:12 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:48:45 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "ft_get_word.h"

char	*ft_get_word(t_env *env, t_read_data *i)
{
	char	*word;
	int		start;

	word = 0;
	start = 0;
	if (env)
	{
		while ((ft_iswhitespace(i->offset[i->c]) || i->offset[i->c] == '-')
				&& i->offset[i->c])
			i->c++;
		start = i->c;
		while (ft_isalnum(i->offset[i->c]) && i->offset[i->c])
			i->c++;
		word = ft_strsub(i->offset, start, (i->c - start));
	}
	return (word);
}
