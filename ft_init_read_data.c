/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_read_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 16:44:57 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/05 12:45:39 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "structure.h"

void	ft_init_read_data(t_read_data *i)
{
	i->i = 0;
	i->c = 0;
	i->current_command = NULL;
	i->map = NULL;
}
