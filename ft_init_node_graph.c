/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_node_graph.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 16:10:33 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/04 16:10:34 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_exit.h"

t_graph	*ft_init_node_graph(t_env *env, int s, int cost, int type)
{
	t_graph	*tmp;

	if (!(tmp = (t_graph *)malloc(sizeof(t_graph))))
		ft_exit(env, EXIT_FAILURE);
	tmp->s = s;
	tmp->type = type;
	tmp->cost = cost;
	tmp->flag = 0;
	tmp->next = NULL;
	return (tmp);
}
