/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_read_data.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/25 12:29:14 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/25 13:34:15 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INIT_READ_DATA_H
# define FT_INIT_READ_DATA_H

# include "structure.h"

void	ft_init_read_data(t_read_data *i);

#endif
