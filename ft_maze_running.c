/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_maze_running.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 15:54:41 by graybaud          #+#    #+#             */
/*   Updated: 2016/03/17 18:59:27 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libftprintf.h>
#include <stdlib.h>
#include "ft_exit.h"
#include "ft_id_to_name.h"

static void		ft_print_ant_queue(t_env *env, t_mapi *tab, int len)
{
	int		i;
	int		c;

	i = len - 1;
	c = 0;
	while (i > 0)
	{
		if (tab[i].id > 0)
		{
			ft_printf("L%d-%s", tab[i].id, ft_id_to_name(env, tab[i].room));
			c = 1;
		}
		if (c)
			ft_printf(" ");
		c = 0;
		--i;
	}
	ft_printf("\n");
}

static t_mapi	*ft_init_ant_queue(t_env *env, int len)
{
	t_mapi	*tmp;
	int		i;

	if (!(tmp = (t_mapi *)malloc(sizeof(t_mapi) * len)))
		ft_exit(env, 1);
	i = 0;
	while (i < len)
	{
		tmp[i].id = -1;
		tmp[i].room = -1;
		++i;
	}
	return (tmp);
}

static void		ft_move_ants(t_env *env, t_mapi *tab, int j, int nb)
{
	tab[j].id = nb;
	tab[j].room = env->shortest->path[j];
}

void			ft_maze_running(t_env *env)
{
	int		j;
	t_mapi	*tab;

	j = 0;
	tab = ft_init_ant_queue(env, env->shortest->nb_node);
	while (tab[env->shortest->nb_node - 1].id != env->ants->id)
	{
		j = 0;
		while (j < env->shortest->nb_node)
		{
			if (tab[j].id == -1)
			{
				ft_move_ants(env, tab, j, 1);
				break ;
			}
			else if (tab[j].id > 0)
			{
				ft_move_ants(env, tab, j, tab[j].id + 1);
				if (tab[j].id > env->ants->id)
					tab[j].id = 0;
			}
			++j;
		}
		ft_print_ant_queue(env, tab, env->shortest->nb_node);
	}
}
